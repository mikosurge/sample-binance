import {
  RESET_TICKER,
  UPDATE_TICKER,
} from '../actions/types';


const INITIAL_STATE = {
  keys: {},
  all: []
};


export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case RESET_TICKER:
      return INITIAL_STATE;
    case UPDATE_TICKER:
      let _all = state.all.slice();
      let _keys = Object.assign({}, state.keys);
      action.payload.forEach(
        row => {
          if (!_keys[row.s]) {
            _keys[row.s] = true;
            _all.push(row);
          } else {
            const idx = _all.findIndex(_row => _row.s === row.s);
            _all[idx] = row;
          }
        }
      );

      return {
        keys: _keys,
        all: _all,
      }
    default:
      return state;
  };
};
