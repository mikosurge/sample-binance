import React, { useState } from 'react';
import styled from 'styled-components';

import Table from '../widget/Table';
import SearchBox from '../widget/SearchBox';
import { tickerColumns } from '../../shared/binance';
import { useSelector } from 'react-redux';


const Styles = styled.div`
  padding: 1rem;
  font-size: 12px;

  table {
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
      td {
        border-bottom: 0;
        }
      }
    }

    th, td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
      border-right: 0;
      }
    }
  }
`;


const TickerTable = props => {
  const columns = React.useMemo(() => tickerColumns, []);
  const data = useSelector(state => state.ticker.all);
  const [searchInput, setSearchInput] = useState('');
  const handleChange = e => setSearchInput(e.target.value);

  const filtered = React.useMemo(() => { return searchInput.length
    ?
      data.filter(
        row => row.s.toLowerCase().includes(searchInput.toLowerCase())
      )
    :
      data;
  },
  [searchInput, data]
  );

  const initialState = React.useMemo(() => ({
    sortBy: [{ id: 's', desc: false }],
    pageSize: 15,
  }), []);

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-12">
          <Styles>
            <SearchBox
              value={searchInput}
              onChange={handleChange}
            />
            <Table
              columns={columns}
              data={filtered}
              initialState={initialState}
            />
          </Styles>
        </div>
      </div>
    </div>
  );
};




export default TickerTable;
