import React from 'react';


const SearchBox = ({ value, onChange }) => {
  return (
    <div className="search-box">
      <label htmlFor="symbol-search">Search for symbol:</label>
      &nbsp;
      <input
        type="text"
        value={value}
        onChange={onChange}
      />
    </div>
  );
};

export default SearchBox;
