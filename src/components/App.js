import React from 'react';

import TickerTable from './ticker/TickerTable';
import WebSocketProvider from '../apis/binanceWs';


class App extends React.Component {
  render() {
    return (
      <WebSocketProvider>
        <TickerTable />
      </WebSocketProvider>
    );
  }
}


export default App;
