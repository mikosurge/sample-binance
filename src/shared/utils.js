export const removeTrailingZeros = str => {
  if (str.includes('.')) {
    let pos = str.length - 1;
    while (str[pos] === '0') {
      pos--;
    }
    if (str[pos] === '.') {
      pos--;
    }
    return str.substring(0, pos + 1);
  }
}


export const sortByParsingFloat = (a, b, col) => {
  return parseFloat(a.values[col]) > parseFloat(b.values[col]) ? 1 : -1;
};
