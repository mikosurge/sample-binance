import {
  RESET_TICKER,
  UPDATE_TICKER,
} from './types';


export const resetTicker = () => {
  return {
    type: RESET_TICKER,
  };
};


export const updateTicker = ticker => {
  return {
    type: UPDATE_TICKER,
    payload: ticker
  };
};
