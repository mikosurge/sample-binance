const D_TICKER = 'TICKER';


export const UPDATE_TICKER = `@@${D_TICKER}/UPDATE_TICKER`;
export const RESET_TICKER = `@@${D_TICKER}/RESET_TICKER`;
