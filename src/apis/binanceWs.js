import React, { createContext } from 'react';
import { useDispatch } from 'react-redux';
import { updateTicker } from '../actions';


const WS_ENDPOINT = "wss://stream.binance.com:9443/ws/!ticker@arr";


const WebSocketContext = createContext(null);
export { WebSocketContext };


const WebSocketProvider = ({ children }) => {
  let socket;
  let ws;

  const dispatch = useDispatch();

  if (!socket) {
    socket = new WebSocket(WS_ENDPOINT);

    socket.onmessage = event => {
      let payload = null;
      try {
        payload = JSON.parse(event.data);
      } catch (error) {
        payload = event.data;
      }
      dispatch(updateTicker(payload));
    };
  }

  ws = {
    socket,
  }

  return (
    <WebSocketContext.Provider value={ws}>
      {children}
    </WebSocketContext.Provider>
  );
};

export default WebSocketProvider;
