# Binance Ticker Table
This web-app opens a websocket that subscribes to a Binance ticker stream, collects data, and provides functionalities for efficiently displaying ticker information.
- Real-time update.
- Symbol searching.
- Field sorting.
- Table pagination.

## Quick Notes
- Due to the different number of records of data emitted from the stream, the app tries to accumulate all of the possible symbols from when it's started. It's not guaranteed that all the known symbols are displayed for the first time.
- No JSON beautify used for stream data (ugly key name) as it should be lean.
- Least style added to make sure the view is clear for users.

## Running the app
The built version is committed for quickly serving, to run this repo immediately, make sure `npm` is installed. Then use these commands:
```bash
npm i -g serve
serve -s build
```
Then head to `localhost:5000` to use the app.


### Development
To run the app in the development environment:
```bash
npm install
npm start
```


## References
### Endpoint
[All Ticker 24h](wss://stream.binance.com:9443/ws/!ticker@arr)

### JSON Format
```json
{
  "e": "24hrTicker",  // Event type
  "E": 123456789,     // Event time
  "s": "BNBBTC",      // Symbol
  "p": "0.0015",      // Price change
  "P": "250.00",      // Price change percent
  "w": "0.0018",      // Weighted average price
  "x": "0.0009",      // First trade(F)-1 price (first trade before the 24hr rolling window)
  "c": "0.0025",      // Last price
  "Q": "10",          // Last quantity
  "b": "0.0024",      // Best bid price
  "B": "10",          // Best bid quantity
  "a": "0.0026",      // Best ask price
  "A": "100",         // Best ask quantity
  "o": "0.0010",      // Open price
  "h": "0.0025",      // High price
  "l": "0.0010",      // Low price
  "v": "10000",       // Total traded base asset volume
  "q": "18",          // Total traded quote asset volume
  "O": 0,             // Statistics open time
  "C": 86400000,      // Statistics close time
  "F": 0,             // First trade ID
  "L": 18150,         // Last trade Id
  "n": 18151          // Total number of trades
}
```

### Documentation
- [Official Documentation](https://github.com/binance-exchange/binance-official-api-docs/blob/master/web-socket-streams.md)
